import React from 'react'

import './wellcome.css'

const Wellcome = (props) => {
	return (
		<section className="Wellcome">
			{props.msg}
		</section>
	)
}

export default Wellcome;