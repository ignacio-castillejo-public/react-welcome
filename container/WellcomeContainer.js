import React, { Component } from 'react';

import Wellcome from '../component/wellcome'
class WellcomeContainer extends Component {

  state = {
    display: true
  };

  render() {
    if(this.state.display) {
      return <Wellcome msg={this.props.msg} />;
    } else{
      return this.props.children;
    }
  }

  componentDidMount() {
    setTimeout(() => this.setState({display: false}), this.props.duration)
  }

}

export default WellcomeContainer;
